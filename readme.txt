TECDEMO ver.0.2  2006/02/25 by T.TAKASU (ttaka@gpspp.sakura.ne.jp)

1.Description

Estimate tec (total electron content) by least-sqaure adjustment using GPS
dual-frequency code observables. Ionosphere is approximated as the single-
layer model at the height of 350km with local or global tec model.
Satellite/receiver dcbs (differential code biases) are estimated as well
as tec model coefficients.


2.Files

testtec.m  : test driver for TECDEMO
tecdemo.m  : TEC estimation demo.
tecmodel.m : TEC model
readrpos.m : read station positions
readrnx.m  : read rinex observation data/navigation message files
gshhs_l.mat: coastline data (noaa gshhs database)


3.Environment

Matlab(5.x,6.x,7.x)


4.More Information

http://gpspp.sakura.ne.jp


5.History

2006/02/24 ver.0.1  new release
2006/02/25 ver.0.2  fix bug of spheric harmonic function Yn0 in tecmodel.m

